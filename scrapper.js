const sleep = async (x) => await new Promise(resolve => setTimeout(resolve, x));
const moment = require('moment')

function waitForNetworkIdle(page, timeout, maxInflightRequests = 0) {
    page.on('request', onRequestStarted);
    page.on('requestfinished', onRequestFinished);
    page.on('requestfailed', onRequestFinished);

    let inflight = 0;
    let fulfill;
    let promise = new Promise(x => fulfill = x);
    let timeoutId = setTimeout(onTimeoutDone, timeout);
    return promise;

    function onTimeoutDone() {
        page.removeListener('request', onRequestStarted);
        page.removeListener('requestfinished', onRequestFinished);
        page.removeListener('requestfailed', onRequestFinished);
        fulfill();
    }

    function onRequestStarted() {
        ++inflight;

        if (inflight > maxInflightRequests)
            clearTimeout(timeoutId);
    }

    function onRequestFinished() {
        if (inflight === 0)
            return;
        --inflight;
        if (inflight === maxInflightRequests)
            timeoutId = setTimeout(onTimeoutDone, timeout);
    }
}

async function scrollPageToBottom(page, fn, scrollStep = 250, scrollDelay = 100) {


    const getScrollHeight = async () => {
        return await page.evaluate(() => {
            if (!document.body) return 0

            const {scrollHeight, offsetHeight, clientHeight} = document.body
            return Math.max(scrollHeight, offsetHeight, clientHeight)
        })
    }

    await page.evaluate(() => {
        window.tweets = []
    })


    const position = await new Promise(async (resolve) => {
        let count = 0
        let x = await page.evaluate(() => {
            let main = document.body.querySelector('main[role=main]')
            return main && main.textContent.includes('This account doesn’t exist')
        })
        let availableScrollHeight = await getScrollHeight()
        if (x)
            resolve(count)
        while (count < availableScrollHeight) {
            count += scrollStep

            await page.evaluate(async (scrollStep) => {
                window.scrollBy(0, scrollStep)
                //await ( async (x) => await new Promise(resolve => setTimeout(resolve, x)))(1000)
                document.body.querySelectorAll('div[data-testid=tweet]').forEach(
                    x => {
                        try {
                            let tweets = x.querySelector('article div[lang]')
                            let datetime;
                            try {
                                datetime = x.querySelector('time[datetime]').attributes['datetime'].value;
                            } catch {
                                datetime = JSON.parse(window.tweets[window.tweets.length - 1]).datetime
                            }
                            if (!tweets)
                                return
                            let text = tweets.textContent
                            let link = x.querySelector('a[href*=status]').href
                            window.tweets.push(JSON.stringify({link, text, datetime}))
                        } catch (e) {
                            console.log('cannot parse tweet')
                        }
                    }
                )
                window.tweets = window.tweets.filter(((value, index, self) => self.indexOf(value) === index));
            }, scrollStep)
            await sleep(scrollDelay)
            availableScrollHeight = await getScrollHeight()
            if (count >= availableScrollHeight) {
                await waitForNetworkIdle(page, 2000);
                await page.evaluate(async () => {
                    window.scrollBy(0, 600)
                })
                count += 600

                availableScrollHeight = await getScrollHeight()
            }
        }

        resolve(count)

    })

    return await page.evaluate(() => window.tweets)
}


async function scanUser(page, user) {

    let totalTweets =await page.evaluate(()=>
        document.querySelector('h2[aria-level="2"][role=heading][dir=auto]')
            .parentNode.querySelector('div[dir=auto]').textContent
    )
    totalTweets = totalTweets.replace(' Tweets','')
    if(totalTweets.includes('K'))
        totalTweets = Number(totalTweets.replace('K','')) * 1000
    else
        totalTweets = Number(totalTweets)


    await scrollPageToBottom(page, 700, 200)
    const tweets = await page.evaluate(x => window.tweets)

    return {
        tweets:
            tweets.map(x => JSON.parse(x)).map(x => ({user: user, ...x, datetime: moment(x.datetime)})),
        total: totalTweets
    }

}

module.exports = {scanUser}