const moment = require('moment')
const _ = require('puppeteer')
const proxyChain = require('proxy-chain');
const ExcelJS = require('exceljs');
const fs = require('fs')
const util = require('util')
const readFile = util.promisify(fs.readFile);
const appendFile = util.promisify(fs.appendFile);
const {Cluster} = require('puppeteer-cluster');

const {scanUser} = require('./scrapper')

const workbook = new ExcelJS.Workbook();
const sheet = workbook.addWorksheet('Tweets');
const dateFrom = process.argv.length === 4 ? moment(process.argv[2]) : moment('2000-01-01');
const dateTo = process.argv.length === 4 ? moment(process.argv[3]) : moment();

async function filter(tweets, keywords) {
    let filtered = []
    for (let tweet of tweets) {
        let found = keywords.filter(x => tweet.text.toLowerCase().includes(x.toLowerCase()))

        if (found.length === 0 || !tweet.datetime.isBetween(dateFrom, dateTo)) continue;

        tweet['keywords'] = found.join(' ')
        filtered.push(tweet)
    }
    return filtered
}

async function writeXls(filtered) {

    const sheet = workbook.worksheets[0];
    sheet.addRows(filtered.map(x => [
        x.user,
        x.user.replace('https://twitter.com/', ''),
        x.link,
        x.text,
        x.keywords,
        x.datetime.format("DD.MM.YYYY")
    ]))

    workbook.xlsx.writeFile('result.xlsx')
        .then(() =>
            console.log(`results written to file (${filtered.length} tweets)`))
        .catch(x => {
            console.log('error writing results')
            console.log(x)
        })


    /*appendFile('result.tsv', filtered.map(
        x =>
            `"${x.user}"\t"${x.user.replace('https://twitter.com/', '')}"\t"${x.link}"\t"${x.text.split('\n').join(' ')}"\t"${}"\t${x.datetime.format("DD.MM.YYYY")}\n`).join('\n'))
        .then(x => {
            console.log(`results written to file (${filtered.length} tweets) `)

        }).catch(x => {
            console.log('error writing results')
            console.log(x)
        })*/

}


(async () => {

    const accounts = (await readFile('accounts', 'utf8')).split('\n').map(x=>x.replace('\r','').trim())
    const config = JSON.parse(await readFile('config.json', 'utf8'))
    const keywords = (await readFile('keywords', 'utf8')).split('\n').map(x=>x.replace('\r','').trim()).filter(x=>x!=='')

    const proxies = config.useProxies ? (await readFile('proxies', 'utf8')).split('\n').map(x=>x.replace('\r','').trim()) : null;

    const proxyArgs = []
    if (proxies)
        for (let i = 0; i < config.chunkSize; i++)
            proxyArgs.push({args: `--proxy-server=${await proxyChain.anonymizeProxy(proxies[i % proxies.length])}`})

    const cluster = await Cluster.launch({
        concurrency: Cluster.CONCURRENCY_CONTEXT,
        maxConcurrency: config.chunkSize,
        timeout: 3600 * 30 * 1000,
        puppeteerOptions: {
            headless: config.headless
        },
        monitor: true,
        perBrowserOptions: proxies ? proxyArgs : undefined
    });


    await cluster.task(async ({page, data: url}) => {
        await page.goto(url, {waitUntil: 'networkidle2'});

        console.log('Started scrapping user ' + url)

        let {tweets,total} = await scanUser(page, url)

        console.log(`Done scrapping user ${url}, found ${tweets.length} tweets, total tweets in account: ${total}`)
        if(tweets.length < Math.min(total,800)){
            console.log(`Adding ${url} to fails.txt`)
            await appendFile('fails.txt',url)
        }

        let filtered = await filter(tweets, keywords)
        await writeXls(filtered)
    });

    cluster.on('taskerror', (err, data) => {
        console.log(`Error on cluster task... ${data}: ${err.message}`);
    });
    accounts.forEach(x => cluster.queue(x))
    await cluster.idle();
    await cluster.close();

})();