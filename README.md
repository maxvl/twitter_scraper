## TwitterScrapperCluster

Перед первым запуском в консоли выполнить ```npm i```  
Запуск без лимита по датам ```node ./main.js```  
Запуск с лимитом по датам ```node ./main.js 2019-01-01 2020-12-31```

#### файлы:
```accounts``` - ссылки на аккаунты в формате ```https://twitter.com/navalny```  
```keywords``` - кейворды, разделенные по линиям  
```proxies``` - прокси, в формате ```schema://[username:password@]host:port```  
```result.xlsx``` - вывод

#### config.json

```chunkSize```: кол-во браузеров, запускаемых одновременно  
```headless```: ```true``` - без рендера страниц, ```false``` - с рендером   
```useProxies```: ```true``` - использовать прокси, ```false``` - не использовать



